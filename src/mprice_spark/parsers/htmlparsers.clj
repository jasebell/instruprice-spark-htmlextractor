(ns mprice-spark.parsers.htmlparsers
  (:require [hickory.core :as hc]
            [hickory.select :as hs]
            [clojure.pprint :as p]
            [clj-time.core :as t]
            [clj-time.format :as tf]))

(defmulti parse-html (fn [html] (:provider html)))

(defmethod parse-html :guitarguitar [html]
  (let [hickory-html (-> (:html html)
                         (hc/parse)
                         (hc/as-hickory))
        items (-> (hs/select
                   (hs/child (hs/attr "itemprop"))
                   hickory-html))
        itemvectors (map (fn [item] (vector (keyword (:itemprop (:attrs item)))
                                            (:content item))) items)
        properties (-> (hs/select (hs/child (hs/attr "property")) html))
        props (map (fn [p] (:attrs p)) properties)]
    ;; check it's a stock url product page
    {:name (first (second (first (filter #(= :name (first %)) itemvectors)))) 
     :price (first (get-in (first (filter (fn [item] (= "price" (get-in item [:attrs :itemprop]))) items)) [:content] ) )
     :rrp-price 0.00
     :on-sale-flag false
     :manu ""
     :colour ""
     :country ""
     :extractdate (tf/unparse (tf/formatter "yyyy-MM-dd hh:mm:ss") (t/now))
     :in-stock 1
     :category ""
     :pageid (:pageid html)
     }
    ))

(defmethod parse-html :absolute [html]
  (let [hickory-html (-> (:html html)
                         (hc/parse)
                         (hc/as-hickory))
        price (-> hickory-html
                  (hs/select
                   (hs/class "regular-price")))
                  itemprops (-> (hs/select (hs/child (hs/attr "itemprop")) hickory-html))]
    {:name (first (get-in (first (filter (fn [item] (= "name" (get-in item [:attrs :itemprop]))) itemprops )) [:content]))
     :price (first (:content (second (:content (first (filter (fn [item] (= "price" (get-in item [:attrs :itemprop]))) itemprops ) )))))
     :rrp-price 0.00
     :on-sale-flag false
     :manu ""
     :colour ""
     :country ""
     :extractdate (tf/unparse (tf/formatter "yyyy-MM-dd hh:mm:ss") (t/now))
     :in-stock 1
     :category ""
     :pageid (:pageid html)
     }
    ))

(defmethod parse-html :andertons [html]
  (let [hickory-html (-> (:html html)
                         (hc/parse)
                         (hc/as-hickory))]
    {:name ""
     :price 0.00
     :rrp-price 0.00
     :on-sale-flag false
     :manu ""
     :colour ""
     :country ""
     :extractdate (tf/unparse (tf/formatter "yyyy-MM-dd hh:mm:ss") (t/now))
     :in-stock 1
     :category ""
     :pageid (:pageid html)
     })
  )

(defmethod parse-html :dawsons [html]
  (let [hickory-html (-> (:html html)
                         (hc/parse)
                         (hc/as-hickory))]
    {:name ""
     :price 0.00
     :rrp-price 0.00
     :on-sale-flag false
     :manu ""
     :colour ""
     :country ""
     :extractdate (tf/unparse (tf/formatter "yyyy-MM-dd hh:mm:ss") (t/now))
     :in-stock 1
     :category ""
     :pageid (:pageid html)
     }
    )
  )

(defmethod parse-html :gak [html]
  (let [hickory-html (-> (:html html)
                         (hc/parse)
                         (hc/as-hickory))]
    {:name ""
     :price 0.00
     :rrp-price 0.00
     :on-sale-flag false
     :manu ""
     :colour ""
     :country ""
     :extractdate (tf/unparse (tf/formatter "yyyy-MM-dd hh:mm:ss") (t/now))
     :in-stock 1
     :category ""
     :pageid (:pageid html)
     }
    )
  )

(defmethod parse-html :gear4music [html]
  (let [hickory-html (-> (:html html)
                         (hc/parse)
                         (hc/as-hickory))
        itemprops (-> (hs/select (hs/child (hs/attr "itemprop")) hickory-html))]
    {:name (first (get-in (second (filter (fn [item] (= "name" (get-in item [:attrs :itemprop]))) itemprops) ) [:content] )) 
     :price (first (:content (first (filter (fn [item] (= "price" (get-in item [:attrs :itemprop]))) itemprops)))) 
     :rrp-price 0.00
     :on-sale-flag false
     :manu (get-in (first (filter (fn [item] (= "brand" (get-in item [:attrs :itemprop]))) itemprops) ) [:attrs :content])
     :colour ""
     :country ""
     :extractdate (tf/unparse (tf/formatter "yyyy-MM-dd hh:mm:ss") (t/now))
     :in-stock (get-in (first (filter (fn [item] (= "inventoryLevel" (get-in item [:attrs :itemprop]))) itemprops) ) [:attrs :content])
     :category ""
     :pageid (:pageid html)
     }
    )
  )

(defmethod parse-html :guitarfxdirect [html]
  (let [hickory-html (-> (:html html)
                         (hc/parse)
                         (hc/as-hickory))]
    {:name ""
     :price 0.00
     :rrp-price 0.00
     :on-sale-flag false
     :manu ""
     :colour ""
     :country ""
     :extractdate (tf/unparse (tf/formatter "yyyy-MM-dd hh:mm:ss") (t/now))
     :in-stock 1
     :category ""
     :pageid (:pageid html)
     })
  )

(defmethod parse-html :musiciansfriend [html]
  (let [hickory-html (-> (:html html)
                         (hc/parse)
                         (hc/as-hickory))]
    {:name ""
     :price 0.00
     :rrp-price 0.00
     :on-sale-flag false
     :manu ""
     :colour ""
     :country ""
     :extractdate (tf/unparse (tf/formatter "yyyy-MM-dd hh:mm:ss") (t/now))
     :in-stock 1
     :category ""
     :pageid (:pageid html)
     })
  )

(defmethod parse-html :thomann [html]
  (let [hickory-html (-> (:html html)
                         (hc/parse)
                         (hc/as-hickory))]
    {:name ""
     :price 0.00
     :rrp-price 0.00
     :on-sale-flag false
     :manu ""
     :colour ""
     :country ""
     :extractdate (tf/unparse (tf/formatter "yyyy-MM-dd hh:mm:ss") (t/now))
     :in-stock 1
     :category ""
     :pageid (:pageid html)
     })
  )
