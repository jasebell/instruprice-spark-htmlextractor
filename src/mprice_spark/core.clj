(ns mprice-spark.core
  (:require [sparkling.core :as spark]
            [sparkling.conf :as conf]
            [sparkling.destructuring :as s-de]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.data.json :as json]
            [hickory.core :as hc]
            [hickory.select :as hs]
            [clojure.pprint :as p]
            [clj-time.core :as t]
            [clj-time.format :as tf]
            [mprice-spark.parsers.htmlparsers :as par])
  (:gen-class))




(defn parse-data [spark-context filepath provider]
  (->> (spark/whole-text-files spark-context filepath)
       (spark/map (s-de/key-value-fn (fn [k v]
                                           (par/parse-html {:provider (keyword provider) :html v :pageid k}))))))

;; Setup the Spark job, just running locally.
;; needs cmd line options plugging in for provider type
(defn -main [& args]
  (let [[prefix] args
        c (-> (conf/spark-conf)
              (conf/master "local[3]")
              (conf/app-name "mpriceparser-sparkjob")
              (conf/set "spark.files.overwrite" "true"))
        sc (spark/spark-context c)]
    (parse-data sc)))

(comment
  (def c (-> (conf/spark-conf)
             (conf/master "local[3]")
             (conf/app-name "mpriceparser-sparkjob")
             (conf/set "spark.hadoop.mapred.output.compress" "true")
             (conf/set "spark.hadoop.mapred.output.compression.codec" "true")
             (conf/set "spark.hadoop.mapred.output.compression.codec" "org.apache.hadoop.io.compress.GzipCodec")
             (conf/set "spark.hadoop.mapred.output.compression.type" "BLOCK")
             (conf/set "spark.files.overwrite" "true")))
  (def sc (spark/spark-context c))

  ;; Step 1 - html -> hickory
  (def ditto (-> (slurp "/Users/jasonbell/ditto.html")
                         (hc/parse)
                         (hc/as-hickory)))

  ;; Step 2 - pull out the itemprop attributes from the html (this is going to change from site to site.
  (def items (-> (hs/select
                  (hs/child
                   (hs/attr "itemprop"))
                  itemprops)))


  (vector (:itemprop (:attrs (first items)))
          (:content (first items)))


  ;; Step 3 - Create a vector of the itemprop name/content pairs.
  (def pairs (map (fn [item]
                    (vector
                     (keyword (:itemprop (:attrs item)))
                     (:content item)))
                  items))



)
