(ns mprice-spark.schemas.extraction )

(defmulti extract-meta :site-name)

(defmethod extract-meta :guitarguitar [content]
  (map (fn [item] (vector (:itemprop (:attrs item))
                          (:content :item))) content))
